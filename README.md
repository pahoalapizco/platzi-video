# Platzi-Video

Proyecto creado durante el curso ReactJS impartido en 
[Platzi](https://platzi.com/cursos/react/) 

## Descripción

Esta mini app es un reproductor de video, en ella se manejan eventos dentro de
los componentes y la API del navegador para manejar ciertas funcionalidades, para
este proyecto se utilizó la funcion para hacer que el video se vea en pantalla
completa.

[Ver aplicación](https://platzi-video.paolacalapizco.now.sh/)

## Instalación:

Descargamos las dependencias que necesita **Platzi-Video** para funcionar 
correctamente.

`npm install` 

## Ejecución
En este proyecto se incluye webpack para la configuración de la app tanto en
desarrollo y producción

### Ejecución en desarrollo

`npm run build:dev`

Inicia la app en el servidor localhost:9000

Para ver corriendo la aplicación en el navegador es necesario descomentar
(en caso de que este comentada) la siguiente linea del archivo index.html

```html
<script src="http://localhost:9000/js/home.js"></script>
```

### Ejecución para producción

`npm run build:prod`

Prepara el proyecto para un entorno de producción, comprimiento todos los JS, JSX
CSS, etc. Y los añade a la carpeta **dist**.

Para que funcione en producción es necesario eliminar/comentar la linea descrita
en > "Ejecución para desarrollo" y despues agregar las siguientes lineas

```html
<link rel="stylesheet" type="text/css" media="screen" href="dist/css/home.css" />
<script src="dist/js/home.js"></script>

```
