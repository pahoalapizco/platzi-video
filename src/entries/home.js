/* ENTRADA REGISTRADA EN WEBPACK... */
// Importan los componentes a utilziar en la aplicación 
import React from 'react' // Crear componentes
// import ReactDOM from 'react-dom' // Renderizar los componentes, Opc 1: Tomamos todo el DOM
import { render } from 'react-dom' // Renderizar los componentes, Opc 2: Tomamos solo el componente render del DOM
import Home from '../pages/containers/home'
import data from '../api.json'

const homeContainer = document.getElementById('home-container')

// ReactDOM.render(Que se va a renderizar, donde se renderizara)
// ReactDOM.render(Componente JSX, contenedor HTML)
// ReactDOM.render(<Media />, app) // Opc 1
/* render( <Media 
            type="video" 
            title="¿Que es React.JS?" 
            author="By PahoAlapizco"
            image="./images/covers/bitcoin.jpg"/>, 
        app ) // Opc 2
 */

render(<Home data={data} />, homeContainer)