import React, { Component } from 'react'
import SearchLayout from '../components/search-layout'

class Search extends Component{
  state = {
    value: '',
  }
  handleSubmit = (event) => {
    event.preventDefault()
    console.log(this.input.value, 'submit!!')
  }
  setInputRef = (element) => {
    this.input = element
  }
  handleInputChange = (event) => {
    this.setState({
      value: event.target.value.replace(' ', '-')
    })
  }
  render(){
    return(
      <SearchLayout 
        handleSubmit={this.handleSubmit} 
        setRef={this.setInputRef}  
        handleChange={this.handleInputChange}
        value={this.state.value}
      />
    )
  }
}

export default Search