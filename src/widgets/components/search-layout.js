import React from 'react'
import './search-layout.css'

// Ej. Componente con una arrow function
const SearchLayout = (props) =>(
  <form 
    className="Search" 
    action=""
    onSubmit={props.handleSubmit}
  >
    <input 
      ref={props.setRef}
      className="Search-input" 
      placeholder="Buscar..."
      type="text"
      onChange={props.handleChange}
      value={props.value}     
    />
  </form>
)
export default SearchLayout