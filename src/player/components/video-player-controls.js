import React from 'react'
import './video-player-constrols.css'

const Controls = (props) => (
	<div className="Controls">
		{props.children}
	</div>
)

export default Controls