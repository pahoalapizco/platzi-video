import React, { Component } from 'react'
import VideoPlayerLayout from '../components/video-player-layout'
import Video from '../components/video'
import Title from '../components/title'
import PlayPause from '../components/play-pausa'
import Timer from '../components/timer'
import Controls from '../components/video-player-controls'
import { FormatTime } from '../../utils/time'
import ProgressBar from '../components/progress-bar'
import Spinner from '../components/spinner'
import Volume from '../components/volume'
import FullScreen from '../components/full-screen'

export default class VideoPlayer extends Component {
  state = {
    pause: true,
    duration: 0,
    currentTime: 0,
    loading: false,
    mute: false,
    volume: 0,
    lastVolume: 0,
  }
  togglePlay = (event) =>{
    this.setState({
      pause: !this.state.pause
    })
  }
  toggleVolume = (event) => {
    this.setState({
      lastVolume: this.video.volume,
      mute: !this.state.mute,     
      volume: this.state.volume===0 ? this.state.lastVolume : 0
    })
    this.video.volume = this.state.lastVolume
  }
  componentDidMount() {
    this.setState({
      pause: (!this.props.autoplay)
    })
  }
  handleLoadedMetadata = (event) => {
    this.video = event.target
    this.setState({
      duration: this.video.duration
    })
  }
  handleTimeUpdate = (event) => {
    this.setState({
      currentTime: this.video.currentTime
    })
  }
  handleProgressChange = (event) => {
    this.video.currentTime = event.target.value
  }
  handleSeeking = (event)=> {
    this.setState({
      loading: true
    })
  }
  handleSeeked = (event) => {
    this.setState({
      loading: false
    })
  }
  handleVolumeChange = (event) => {
    this.video.volume = event.target.value
    this.setState({
      volume: this.video.volume
    })
  }
  handleFullScreemClick = (event) => {
    if (!document.webkitIsFullScreen) {
      // mando a full screen
      this.player.webkitRequestFullscreen()
    } else {
      document.webkitExitFullscreen()
    }
  }
  setRef = (element) => {
    this.player = element
  }

  render(){
    return(
      <VideoPlayerLayout setRef={this.setRef}>
        <Title title={this.props.title}/>
        <Controls>
          <PlayPause 
            pause={this.state.pause}
            handleClick={this.togglePlay}
          />
          <Timer 
            currentTime={FormatTime(this.state.currentTime)} 
            duration={FormatTime(this.state.duration)} 
          />
          <ProgressBar 
            duration={this.state.duration}
            value={this.state.currentTime}
            handleProgressChange={this.handleProgressChange}
          />
          <Volume 
            mute={this.state.mute}
            handleVolumeChange={this.handleVolumeChange}
            handleVolumeClick={this.toggleVolume}
          />
          <FullScreen 
            handleFullScreemClick={this.handleFullScreemClick}
          />
        </Controls>
        {
          this.state.loading && <Spinner />
        }
        <Video 
          autoplay={this.props.autoplay} 
          pause={this.state.pause}
          src={this.props.src}
          handleLoadedMetadata={this.handleLoadedMetadata}
          handleTimeUpdate={this.handleTimeUpdate}
          handleSeeking={this.handleSeeking}
          handleSeeked={this.handleSeeked}
        />
      </VideoPlayerLayout>
    )
  }
}