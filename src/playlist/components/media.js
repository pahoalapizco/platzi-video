import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import './media.css'
// Medía es una clase que represneta un componente, el cual se utilizara en nuestra aplicación web!!
// Nombre del componente SIEMPRE debe ir en Mayuscula, para que react sepa que ese es un Componente!
class Media extends PureComponent {
	constructor (props) {
		super(props) // Accedemos alos elementos del "props"
			// Esta es la manera de hacerlo con ECMAScript6, un tanto arcaica, pues por cada evento 
		//se tendria que hacer el bind de this con ek evento correspondiente
		// this.handleClick = this.handleClick.bind(this) //enlazamos this con la función handleClick
	}

	handleClick = (event) => {
		this.props.openModal(this.props)
	}
	render(){        
		return(
			<div className="Media" onClick={this.handleClick}> 
				<div className="Media-cover">
					<img className="Media-image"
						src = { this.props.cover }
						alt = ""
						width  = {240}
						height = {160}
					/>
					<h3 className="Media-title ">{ this.props.title } </h3>
					<p className="Media-author"> { this.props.author } </p>
				</div>
			</div>
		)
	}
}

Media.propTypes = {
	cover: PropTypes.string,
	title: PropTypes.string.isRequired,
	author: PropTypes.string,
	type: PropTypes.oneOf(['video', 'audio'])
}

export default Media