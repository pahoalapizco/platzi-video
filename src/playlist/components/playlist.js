import React, { Component } from 'react'
import Media from './media'
import './playlist.css'

// Componente funcional: Es una función, no necesita el cliclo de vida de un componente .
function Playlist (props){
    return(
      <div className="Playlist"> 
        {
          props.playlist.map( (item) => {
            return  <Media {...item} key={item.id} openModal={props.handleOpenModal} />
          })
        }
      </div>
    )
}
export default Playlist