import React, { Component } from 'react'
import './home-layout.css'
// Componente "tonto": en el se añaden los estilos que se mostraran en el SC (Smart Component)
// Componente = Componentes tonto
function HomeLayot(props){
	return(
		<section className="HomeLayout">
			{props.children}
		</section>
	)
}

export default HomeLayot